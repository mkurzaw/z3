#include "Matrix.hh"

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Matrix, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::istream &operator>>(std::istream &stream, Matrix &matrix) //przeciazenie dla strumienia wejsciowego
{
    for(int i=0; i<SIZE; i++)
        stream>>matrix[i];
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const Matrix &matrix) //przeciazenie dla strumienia wyjsciowego
{
    for(int i=0; i<SIZE; i++)
        stream<<matrix[i]<<std::endl;
    return stream; 
}


  void Matrix::Gauss() //metoda oblicza wyznacznik metoda gaussa
  {
      Matrix tmp=*this; /// this wskaznik na samego siebie kopiujemy tu macierz do macierzy pomocnieczej
      double parity=1;
      for(int i=0; i<SIZE; i++)
      {
          for(int j=i+1; j<SIZE && tmp[i][i]==0; j++  )
          {
              parity=-parity;
              std::swap(tmp[i],tmp[j]);
          }
      
      if(tmp[i][i]==0)
      {
          determinant=0;
      }
      
      determinant=parity;
      for(int j=i+1; j<SIZE; j++)
      {
          tmp[j]= tmp[j]-tmp[i]*(tmp[j][i]/tmp[i][i]);
         
      } 
      }
 for(int i=0; i<SIZE; i++)
      determinant=determinant*tmp[i][i];
    

  }


    Vector Matrix::operator * (Vector B)
    {
        Vector result;
        for(int i=0; i<SIZE; i++)
        {
        result[i]=0;
            for(int j=0; j<SIZE; j++)
             result[i]= result[i]+M[j][i]*B[j];
        }
        return result;


    }
