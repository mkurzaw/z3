#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */

int main()
{
  SystemOfLinearEquations system; // dzialanie

  cout << endl
       << " Start programu " << endl;
  cin>>system; //wczytanie danych
  cout<<system<< endl; //wyswietlenie przeciazonego wyjscia
  cout<<"result"<<endl;
  switch (system.calculate())
  {
  case 0: // dla nieskonczenie wielu rozwiazan
   cout<<"nieskonczenie wiele rozwoazan"<<endl;
    break;
    case 1: //brak rozw
   cout<<"brak rozwiazan"<<endl;
    break;
  case 2: //dla 3 rozwiazan rzeczywistych
   cout<<system.get_result_vector()<<endl; 
   system.Error();
   cout<<"wektor bledu : "<<system.get_error_vector()<<endl;
     cout<<"dlugosc wektora bledu : "<<system.get_error()<<endl;
    break;

  }
}
/* Mikolaj Kurzwski 18.04.2020 Rozwiazywanie rowan liniowych nr indeksu:252894
Program ma za zadanie wczytac macierz wpolczynnikow rownan wielkosci 3x3 a nastepnie wektor wyrazow wolnych. 
Macierze musza byc wczytane w postaci transponowanej.
Na wyjsciu programu pokaze sie macierz wspolczynniokow w postaci ogolnej, nastepnie zostana wyswietlone wyrazy wolne, nastepnie wyrazy szukane, a na koniec dlugosc wektora bledu i jego wartosc.
test:
Wprowadzone dane:
2 1 1
2 2 3
1 1.5 1
9 8.5 8
Dane wyswietlone:
A^T
2	1	1	
2	2	3	
1	1.5	1	

vector words available
9	8.5	8	

result
2	1	3	
wektor bledu : 0	0	0	
dlugosc wektora bledu : 0


*/
