#include "SystemOfLinearEquations.hh"

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy SystemOfLinearEquations, ktore zawieraja 
 *  wiecej kodu niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system) //przeciazenie wejscia dla dzialania
{
    stream>>system.set_factor()>>system.set_vector_words_available(); //wczytywanie macierzy najpierw wspolczynnikow nastepnie wektor wyrazow wolnych
    return stream;
}


std::ostream &operator<<(std::ostream &stream,const SystemOfLinearEquations &system) // przeciazenie dla wyjscia
{
stream<<"A^T"<<std::endl; //macierz jest wczytywana w postaci transponowanej i zostaje wyswietlona w normalnej
    stream<<system.get_factor()<<std::endl;
    stream<<"vector words available"<<std::endl; // wyswietlenie  wektorow wyrazow wolnych
    stream<<system.get_vector_words_available()<<std::endl;
    return stream;

}


void SystemOfLinearEquations::Error() //funkcja wyznaczjaca blad obliczeniowy
{
    error_vector=factor*result_vector-vector_words_available; //oblicznie dlugosci wektora bledu
    error=sqrt(error_vector*error_vector); // wyliczenie bledu
}

   int SystemOfLinearEquations::calculate() // obliczanie wyznacznikow
   {

     factor.Gauss();
     Matrix tmp;
     double determinant= factor.get_determinant();

     if(determinant==0)
     {
        for(int i=0; i<SIZE;i++)
        {
            tmp=factor;
            tmp[i]=vector_words_available;
            tmp.Gauss();
            if( tmp.get_determinant()==0) /// 0/0 nieskonczenie wiele rozewiazan
                return 0;
        }
        return 1; /// kiedy tylko wyznacznik glownej macierzy jest rowny 0 to nie ma rozwiazan
     }

        for(int i=0; i<SIZE;i++)
        {
            tmp=factor;
            tmp[i]=vector_words_available;
            tmp.Gauss();
            
            result_vector[i]=tmp.get_determinant()/determinant;
        }


     return 2; // gdy sa rozwiazania

   }
