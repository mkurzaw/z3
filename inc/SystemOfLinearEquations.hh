#pragma once

#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include <cmath>
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class SystemOfLinearEquations
{
  Matrix factor; // factor - wspolczniniki
  Vector vector_words_available; //wektor wyrazow wolnych
  Vector result_vector; // wektor wyrazow szukanych
  Vector error_vector; //dlugosc wektora bledu
  double error; //blad oobliczen
public:

   Matrix get_factor()const {return factor;} //funkcja zwracajaca wspolczynnik
   Vector get_result_vector()const {return result_vector;} //funkcja zwracajaca wyrazy szukane
     Vector get_error_vector()const {return error_vector;} //funkcja zwracajaca wektor bledu
       double get_error()const {return error;} //funkcja zwracajaca blad obliczen
   Vector get_vector_words_available()const {return vector_words_available;} //funkcja zwracajaca wektor wyrazow wolnych
   Matrix &set_factor() {return factor;} //funkcja wczytujaca wspolczynniki
   Vector &set_vector_words_available() {return vector_words_available;} //funkcja wczytujaca wyrazy wolna
   void Error(); 
   int calculate();
};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system); 

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream,
                        const SystemOfLinearEquations &system);
