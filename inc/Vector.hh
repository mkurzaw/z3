#pragma once

#include "Size.hh"
#include <iostream>

/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Vector
{
  double tab[SIZE]; //tablica dla 3 wektorow
public:
double & operator[](int i)
  {
    return tab[i];
  }

  double operator[](int i) const
  {
    return tab[i];
  }
    //przeciazenia dzialan dla wektorow
       Vector operator +( Vector B);
       Vector operator -( Vector B);
       double operator *( Vector B);
       Vector operator *( double B);
       Vector operator /( double B);

/*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, Vector &vec); //przeciazenie strumienia wejsciowego

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream, const Vector &vec); //przeciazenie strumienia wyjsciowego
