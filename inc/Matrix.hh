#pragma once

#include "Size.hh"
#include "Vector.hh"
#include <iostream>
#include <algorithm>
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class Matrix
{
  Vector M[SIZE];
  double determinant; //determinant-wyznacznik
  public:
   
   double get_determinant() // funkcja wyznacznia wyznacznika
   {
    return determinant;
   }

    Vector & operator[](int i) 
  {
    return M[i];
  }

  Vector operator[](int i) const //przeciazenie operetora [] tak aby zwracal wartosc z pola klasy na zewnatrz
  {
    return M[i];
  }  

  void Gauss();
  
  Vector operator * (Vector B);
  
};

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::istream &operator>>(std::istream &stream, Matrix &matrix);

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
std::ostream &operator<<(std::ostream &stream, const Matrix &matrix);
